package br.com.ifpb.praticas.projeto.praticas.comandos;

import br.com.ifpb.praticas.projeto.praticas.core.Projeto;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author douglasgabriel
 * @author Emanuel Batista
 * @version 1.0
 */
public class ComandosGit {

    private Projeto projeto;
    private List<String> prefix = new ArrayList<>();

    public ComandosGit(Projeto projeto) {
        this.projeto = projeto;
        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            prefix.add("cmd.exe");
            prefix.add("/c");
            prefix.add("git");
        } else {
            prefix.add("git");
        }
    }

    public Process init() throws IOException {
        prefix.add("init");
        Process process = new ProcessBuilder().directory(new File(projeto.getPath())).command(prefix).start();
        prefix.remove("init");
        return process;
    }

    public Process add() throws IOException {
        prefix.add("add");
        prefix.add("--all");
        Process process = new ProcessBuilder().directory(new File(projeto.getPath())).command(prefix).start();
        prefix.remove("add");
        prefix.remove("--all");
        return process;
    }

    public Process commit(String menssagem) throws IOException {
        this.add();
        prefix.add("commit");
        prefix.add("-m");
        prefix.add(menssagem);
        Process process = new ProcessBuilder().directory(new File(projeto.getPath())).command(prefix).start();
        prefix.remove("commit");
        prefix.remove("-m");
        prefix.remove(menssagem);
        return process;
    }

    public Process pull(String remote, String branch) throws IOException {
        prefix.add("pull");
        prefix.add(remote); 
        prefix.add(branch);
        Process process = new ProcessBuilder().directory(new File(projeto.getPath())).command(prefix).start();
        prefix.remove("pull");
        prefix.remove(remote); 
        prefix.remove(branch);
        return process;
    }

    public Process push(String remote, String branch) throws IOException {
        prefix.add("push");
        prefix.add(remote); 
        prefix.add(branch);
        Process process = new ProcessBuilder().directory(new File(projeto.getPath())).command(prefix).start();
        prefix.remove("push");
        prefix.remove(remote); 
        prefix.remove(branch);
        return process;
    }

}
