package br.com.ifpb.praticas.projeto.praticas.core;

import java.io.File;

/**
 *
 * @author DouglasGabriel
 * @author Emanuel Batista
 */
public class Projeto {
    
    private String path;
    private TipoProjeto tipoProjeto;
    private TipoGerenciador tipoGerenciador;
    private String mainClass;
    private File arquivoEditado;  

    public String getMainClass() {
        return mainClass;
    }

    public void setMainClass(String mainClass) {
        this.mainClass = mainClass;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public File getArquivoEditado() {
        return arquivoEditado;
    }

    public void setArquivoEditado(File arquivoEditado) {
        this.arquivoEditado = arquivoEditado;
    }

    public TipoProjeto getTipoProjeto() {
        return tipoProjeto;
    }

    public void setTipoProjeto(TipoProjeto tipoProjeto) {
        this.tipoProjeto = tipoProjeto;
    }

    public TipoGerenciador getTipoGerenciador() {
        return tipoGerenciador;
    }

    public void setTipoGerenciador(TipoGerenciador tipoGerenciador) {
        this.tipoGerenciador = tipoGerenciador;
    }
    
    
    
}
