package br.com.ifpb.praticas.projeto.praticas.comandos;

import br.com.ifpb.praticas.projeto.praticas.core.Projeto;
import br.com.ifpb.praticas.projeto.praticas.core.TipoProjeto;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author DouglasGabriel
 */
public class ComandoAnt implements Comandos{
    
    private String prefix = "";
    private String javaPrefix = "";
    private Projeto projeto;
    
    public ComandoAnt (Projeto projeto){
        this.projeto = projeto;
        if ((System.getProperty("os.name").toLowerCase().contains("win"))){
            this.prefix = "cmd.exe /c ant -buildfile \""+projeto.getPath()+File.separator+"build.xml\" ";
        }                        
        else{
            this.prefix = "ant -buildfile \""+projeto.getPath()+File.separator+"build.xml\" ";
        }
        
    }
    
    @Override
    public Process limpar () throws IOException{
        return Runtime.getRuntime().exec(prefix + "clear");
    }
    
    @Override
    public Process construir () throws IOException{
        if (projeto.getTipoProjeto().equals(TipoProjeto.DESKTOP))
            return Runtime.getRuntime().exec(prefix + "build");
        else 
            return Runtime.getRuntime().exec(prefix + "buildWeb");
    }
    
    /**
     *
     * @param mainClass
     * @return
     * @throws IOException
     */
    @Override
    public Process empacotarJar (String mainClass) throws IOException{
        File file = new File(projeto.getPath());
        return Runtime.getRuntime().exec(prefix + "packageJar -DpackageName=" + file.getName().replace(" ", "-") + ".jar -DMain=" + mainClass);
    }
    
    @Override
    public Process executarJar () throws IOException{
        File file = new File(projeto.getPath());
        return Runtime.getRuntime().exec(prefix + "executeJar -DpackageName=" + file.getName().replace(" ", "-"));
    }
    
    @Override
    public Process empacotarWar () throws IOException{
        File file = new File(projeto.getPath());
        return Runtime.getRuntime().exec(prefix + "packageWar -DpackageName=" + file.getName().replace(" ", "-"));
    }
    
    @Override
    public Process executarClasse (String classe) throws IOException{
        return Runtime.getRuntime().exec(prefix + "executar -DMain=" +classe);
    }
    
    @Override
    public Process deployWar (String serverPath) throws IOException{
        File file = new File(projeto.getPath());
        return Runtime.getRuntime().exec(prefix + "deploy -Dserver=\"" + serverPath + "\" -DpackageName=" + file.getName().replace(" ", "-"));
    }
    
    @Override
    public Process executarServidor (String serverPath) throws IOException{        
        return Runtime.getRuntime().exec(prefix + "executeServer -Dtomcat.home=" + serverPath);
    }        
    
    @Override
    public Process pararServidor (String serverPath) throws IOException{
        return Runtime.getRuntime().exec(prefix + "stopServer -Dtomcat.home=" + serverPath);
    }

    @Override
    public Process compilar() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    
}