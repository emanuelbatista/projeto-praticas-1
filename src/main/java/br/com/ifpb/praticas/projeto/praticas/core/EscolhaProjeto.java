package br.com.ifpb.praticas.projeto.praticas.core;

import br.com.ifpb.praticas.projeto.praticas.gerenciador.Gerenciador;
import br.com.ifpb.praticas.projeto.praticas.gerenciador.GerenciadorAnt;
import br.com.ifpb.praticas.projeto.praticas.gerenciador.GerenciadorMaven;
import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Emanuel Batista da Silva Filho - emanuelbatista2011@gmail.com
 */
public class EscolhaProjeto {
    private File diretorio;
    private Projeto projeto;
    private Gerenciador gerenciador;
    
    public EscolhaProjeto(File diretorio) {
      this.diretorio=diretorio;
      this.projeto=new Projeto();
      
      configurarProjeto();
    }
    
    private void configurarProjeto(){
        switch(getTipoGerenciador()){
            case MAVEN:{
                gerenciador=new GerenciadorMaven(diretorio);
            }break;
            case ANT:{
                gerenciador=new GerenciadorAnt(diretorio);
            }break;
        }
        projeto.setTipoProjeto(gerenciador.getTipoProjeto());
        projeto.setPath(diretorio.getAbsolutePath());
    }
    
    private TipoGerenciador getTipoGerenciador(){
        List<String> names=Arrays.asList(diretorio.list());
        if(names.contains("pom.xml") ){
            projeto.setTipoGerenciador(TipoGerenciador.MAVEN);
        }
        if(names.contains("build.xml") && names.contains("src") && names.contains("lib")){
            projeto.setTipoGerenciador(TipoGerenciador.ANT);
        }
        return projeto.getTipoGerenciador();
    }
    
    public Projeto getProjeto(){
        return this.projeto;
    }
    
    
}
