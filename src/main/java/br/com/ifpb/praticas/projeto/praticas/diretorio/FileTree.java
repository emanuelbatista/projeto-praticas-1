/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.praticas.projeto.praticas.diretorio;

import java.io.File;

  public class FileTree extends File{

        public FileTree(File parent, String child) {
            super(parent, child);
        }

        public FileTree(String pathname) {
            super(pathname);
        }
        
        @Override
        public String toString() {
            return getName();
        }
        
        
    }
