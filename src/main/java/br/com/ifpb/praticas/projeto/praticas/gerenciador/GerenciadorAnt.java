/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.praticas.projeto.praticas.gerenciador;

import br.com.ifpb.praticas.projeto.praticas.core.TipoProjeto;
import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Emanuel Batista da Silva Filho - emanuelbatista2011@gmail.com
 */
public class GerenciadorAnt implements Gerenciador{

    private File diretorio;
    
    public GerenciadorAnt(File diretorio){
        this.diretorio=diretorio;
    }
    
    @Override
    public TipoProjeto getTipoProjeto() {
        List<String> names=Arrays.asList(diretorio.list());
        if(names.contains("web")){
            return TipoProjeto.WEB;
        }else{
            return TipoProjeto.DESKTOP;
        }
    }
    
}
