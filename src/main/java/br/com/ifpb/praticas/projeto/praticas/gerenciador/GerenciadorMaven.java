/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.praticas.projeto.praticas.gerenciador;

import br.com.ifpb.praticas.projeto.praticas.core.TipoProjeto;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 *
 * @author Emanuel Batista da Silva Filho - emanuelbatista2011@gmail.com
 */
public class GerenciadorMaven implements Gerenciador {

    private File diretorio;

    public GerenciadorMaven(File diretorio) {
        this.diretorio = diretorio;
    }

    @Override
    public TipoProjeto getTipoProjeto() {
        try {
            File arquivoPom
                    = new File(diretorio.getAbsolutePath() + File.separator + "pom.xml");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(arquivoPom);
            Element root = document.getDocumentElement();
            Element packaging = (Element) root.getElementsByTagName("packaging").item(0);
            if (packaging.getTextContent().equals("war")) {
                return TipoProjeto.WEB;
            }
            if (packaging.getTextContent().equals("jar")) {
                return TipoProjeto.DESKTOP;
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(GerenciadorMaven.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

}
