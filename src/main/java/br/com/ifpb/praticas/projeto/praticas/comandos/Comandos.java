package br.com.ifpb.praticas.projeto.praticas.comandos;

import java.io.IOException;

/**
 *
 * @author douglasgabriel
 */
public interface Comandos {

    Process limpar() throws IOException;

    Process construir() throws IOException;

    Process compilar() throws IOException;

    Process empacotarJar(String mainClass) throws IOException;

    Process executarJar() throws IOException;

    Process empacotarWar() throws IOException;

    Process executarClasse(String classe) throws IOException;

    Process deployWar(String serverPath) throws IOException;

    Process executarServidor(String serverPath) throws IOException;

    Process pararServidor(String serverPath) throws IOException;

}
