package br.com.ifpb.praticas.projeto.praticas.diretorio;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTree;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * Classe que vai organizar toda a estrutura de diretória para 
 * ser mostrada pelo {@link JTree} 
 * 
 * @author Emanuel Batista da Silva Filho
 */
public class SistemaArvoreArquivo implements TreeModel {

    private File raiz;
    private List<TreeModelListener> lista;

    public SistemaArvoreArquivo(File raiz) {
        this.raiz = raiz;
        this.lista = new ArrayList<>();
    }

    @Override
    public Object getRoot() {
        return this.raiz;
    }

    @Override
    public Object getChild(Object parent, int index) {
        File pai = (File) parent;
        String[] filhos = pai.list();
        return new FileTree(pai, filhos[index]);
    }

    @Override
    public int getChildCount(Object parent) {
        File pai = (File) parent;
        if (pai.isDirectory() && pai.list() != null) {
            return pai.list().length;
        } else {
            return 0;
        }
    }

    @Override
    public boolean isLeaf(Object node) {
        return ((File) node).isFile();
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        File oldFile = (File) path.getLastPathComponent();
        String fileParentPath = oldFile.getParent();
        String newFileName = (String) newValue;
        File targetFile = new File(fileParentPath, newFileName);
        oldFile.renameTo(targetFile);
        File parent = new File(fileParentPath);
        int[] changedChildrenIndices = {getIndexOfChild(parent, targetFile)};
        Object[] changedChildren = {targetFile};
        fireTreeNodesChanged(path.getParentPath(), changedChildrenIndices, changedChildren);

    }

    private void fireTreeNodesChanged(TreePath parentPath, int[] indices, Object[] children) {
        TreeModelEvent event = new TreeModelEvent(this, parentPath, indices, children);
        for (TreeModelListener item: lista) {
            item.treeNodesChanged(event);
        }
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        File pai=(File) parent;
        File filho=(File) child;
        String[] nomeFilhos=pai.list();
        for (int i=0;i<nomeFilhos.length;i++) {
            if(filho.getName().equals(nomeFilhos[i])){
                return i;
            }
        }
        return -1;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        lista.add(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        lista.add(l);
    }
    

}
