/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.praticas.projeto.praticas.gerenciador;

import br.com.ifpb.praticas.projeto.praticas.core.TipoProjeto;

/**
 *
 * @author Emanuel Batista da Silva Filho - emanuelbatista2011@gmail.com
 */
public interface Gerenciador {
    
    public TipoProjeto getTipoProjeto();
    
}
