package br.com.ifpb.praticas.projeto.praticas.comandos;

import br.com.ifpb.praticas.projeto.praticas.core.Projeto;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author douglasgabriel
 * @version 0.1
 */
public class ComandoMaven implements Comandos {

    private String[] prefix;
    private Projeto projeto;

    public ComandoMaven(Projeto projeto) {
        this.projeto = projeto;
        this.prefix = (System.getProperty("os.name").toLowerCase().contains("win"))
                ? "cmd.exe /c mvn".split(" ")
                : "mvn".split(" ");
    }

    @Override
    public Process limpar() throws IOException {
        List<String> comandos = new ArrayList<>();
        for (String s : prefix) {
            comandos.add(s);
        }
        comandos.add("clean");
        return new ProcessBuilder().directory(new File(projeto.getPath())).command(comandos).start();
    }

    @Override
    public Process construir() throws IOException {
        List<String> comandos = new ArrayList<>();
        for (String s : prefix) {
            comandos.add(s);
        }
        comandos.add("install");
        return new ProcessBuilder().directory(new File(projeto.getPath())).command(comandos).start();
    }

    /**
     * Para executar uma classe principal faz-se necessário um plugin, declarado
     * da seguinte forma:
     * <plugin>
     * <artifactId>maven-assembly-plugin</artifactId>
     * <configuration>
     * <archive>
     * <manifest>
     * <mainClass>${exec.mainClass}</mainClass>
     * </manifest>
     * </archive>
     * <descriptorRefs>
     * <descriptorRef>jar-with-dependencies</descriptorRef>
     * </descriptorRefs>
     * </configuration>
     * <executions>
     * <execution>
     * <id>create-archive</id>
     * <phase>package</phase>
     * <goals>
     * <goal>single</goal>
     * </goals>
     * </execution>
     * </executions>
     * </plugin>
     * Sendo assim, é necessário, antes de executar, adicionar este plugin no
     * pom.xml
     */
    @Override
    public Process empacotarJar(String mainClass) throws IOException {
        List<String> comandos = new ArrayList<>();
        for (String s : prefix) {
            comandos.add(s);
        }
        comandos.add("package");
        comandos.add("-Dexec.mainClass=" + mainClass);
        return new ProcessBuilder().directory(new File(projeto.getPath())).command(comandos).start();

    }

    @Override
    public Process executarJar() throws IOException {
        this.construir();
        List<String> comandos = new ArrayList<>();
        for (String s : prefix) {
            comandos.add(s);
        }
        String[] arquivos = new File(projeto.getPath() + File.separator + "target").list(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                return name.contains("jar-with-dependencies.jar");

            }
        });
        comandos.add("exec:exec");
        comandos.add("-Dfile.jar=" + arquivos[0]);
        return new ProcessBuilder().directory(new File(projeto.getPath())).command(comandos).start();
    }

    @Override
    public Process empacotarWar() throws IOException {
        List<String> comandos = new ArrayList<>();
        for (String s : prefix) {
            comandos.add(s);
        }
        comandos.add("package");
        return new ProcessBuilder().directory(new File(projeto.getPath())).command(comandos).start();
    }

    /**
     * Para executar uma classe principal faz-se necessário um plugin, declarado
     * da seguinte forma:
     * <plugin>
     * <groupId>org.codehaus.mojo</groupId>
     * <artifactId>exec-maven-plugin</artifactId>
     * <version>1.2.1</version>
     * <executions>
     * <execution>
     * <goals>
     * <goal>java</goal>
     * </goals>
     * </execution>
     * </executions>
     * <configuration>
     * <mainClass>${exec.mainClass}</mainClass>
     * <arguments>
     * <argument>foo</argument>
     * <argument>bar</argument>
     * </arguments>
     * </configuration>
     * </plugin>
     * Sendo assim, é necessário, antes de executar, adicionar este plugin no
     * pom.xml
     */
    @Override
    public Process executarClasse(String classe) throws IOException {
        this.construir();
        List<String> comandos = new ArrayList<>();
        for (String s : prefix) {
            comandos.add(s);
        }
        comandos.add("exec:java");
        comandos.add("-Dexec.mainClass=" + classe);
        return new ProcessBuilder().directory(new File(projeto.getPath())).command(comandos).start();
    }

    @Override
    public Process deployWar(String serverPath) throws IOException {
        List<String> comandos = new ArrayList<>();
        for (String s : prefix) {
            comandos.add(s);
        }
        String[] arquivos = new File(projeto.getPath() + File.separator + "target").list(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                return name.contains(".war");

            }
        });
        comandos.add("jetty:deploy-war");
        comandos.add("-Dfile.war=" + arquivos[0]);
        return new ProcessBuilder().directory(new File(projeto.getPath())).command(comandos).start();
    }

    @Override
    public Process executarServidor(String serverPath) throws IOException {
        List<String> comandos = new ArrayList<>();
        for (String s : prefix) {
            comandos.add(s);
        }
        comandos.add("jetty:start");
        return new ProcessBuilder().directory(new File(projeto.getPath())).command(comandos).start();
    }

    @Override
    public Process pararServidor(String serverPath) throws IOException {
        List<String> comandos = new ArrayList<>();
        for (String s : prefix) {
            comandos.add(s);
        }
        comandos.add("jetty:stop");
        return new ProcessBuilder().directory(new File(projeto.getPath())).command(comandos).start();
    }

    @Override
    public Process compilar() throws IOException {
        List<String> comandos = new ArrayList<>();
        for (String s : prefix) {
            comandos.add(s);
        }
        comandos.add("compile");
        return new ProcessBuilder().directory(new File(projeto.getPath())).command(comandos).start();
    }
}
