package br.com.ifpb.praticas.projeto.praticas.core;

/**
 *
 * @author DouglasGabriel
 */
public enum TipoProjeto {
    
    WEB("web"), DESKTOP("desktop");
        
    private String tipo;

    private TipoProjeto(String tipo) {
        this.tipo = tipo;
    }        
    
    public String getTipo(){
        return this.tipo;
    }
    
}
