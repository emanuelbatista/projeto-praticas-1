#Projeto de Práticas de Programação - IDE#

Este projeto consiste na criação de uma IDE capaz de dar suporte
a projetos WEB e Desktop, desde a edição de arquivos até a execução
e implantação em servidores.

##Desenvolvedores##

- Douglas Gabriel Gouveia - <douglasxgabriel@gmail.com>;
- Emanuel Batista         - <emanuelbatista2011@gmail.com>;

##Tecnologias##

- Java SE;
- Swing;
- Ant;
- Maven;
- Git;

##Funcionamento##

Ao executar o projeto, através do Jar ou do código-fonte executado
em qualquer IDE com suporte ao Maven, será possível abrir um projeto
java com as seguintes estruturas:

####Ant####

> Web
> > src
> >
> > lib

> > web

> > build.xml

ou

> Desktop
> > src

> > lib

> > build.xml

####Maven####

> Web ou Desktop
> > src

> > pom.xml

Com o projeto aberto, a IDE oferecerá várias opções em sua barra superior
apenas bastando navegar pelas guias e selecionar as opções de execução
ou mesmo comandos Git.

####Funcionalidades####

- Limpar;
- Construir;
- Compilar (Projeto Maven);
- Executar Jar (Projeto Desktop);
- Executar Classe (Projeto Desktop);
- Empacotar;
- Implantar War (Projeto Web);
- Iniciar servidor (Projeto Web);
- Parar servidor (Projeto Web);
- Inicializar repositório Git;
- Fazer commit (Projeto Git);
- Fazer Pull (Projeto Git);
- Fazer Push (Projeto Git);

> **OBS:** Para realizar os comandos git push e pull, é necessário cadastrar previamente uma chave pública ssh para o repositório.